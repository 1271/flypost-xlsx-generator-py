# XLSX генератор

Структура входных данных:

```json
{
    "payload": "",
    "formats": {
        "fmt1": {
            "font": {
                "bold": true,
                "italic": true,
                "alignment": "center",
                "color": "#f0a0c0"
            },
            "border": {
                "left": {
                    "color": "#FF0000",
                    "type": 1
                }
            }
        }
    },
    "sheets": {
        "book 1": {
            "0": {
                "2": {
                    "value": "tExt",
                    "format": "fmt1"
                },
                "3": {
                    "value": "=c1",
                    "formula": "refresh it, please"
                },
                "4": "=CONCAT(C1, C2)"
            },
            "1": {
                "2": {
                    "value": "C2 text",
                    "format": "fmt1"
                }
            },
            "2": {
                "2": {
                    "value": "a123"
                },
                "5": "value cell 5 row 2",
                "6": 12345
            }
        }
    }
}
```
**payload**, **formats** - `опциональные поля`

_содержимое payload будет возвращено при изменении статуса задачи_

```json
{
  "taskId": "<id>",
  "status": "queue",
  "payload": "<payload>"
}
```

При создании задачи поле не возвращается.


**Внимание! Формулы не подвергаются просчетам.**
```json
{"formula": "default value"}
```
Данный текст будет размещен в ячейке по умолчанию (программы просмотра XLSX должны автоматически пересчитать ячейки).
Данное поведение считается рекомендованным.
Ключ **formula** не обязательный.


**Border types:**
```
1 - линия
2 - жирная линия
3 - прерывистые линии
4 - точки
```


Ответ от сервера (при создании задачи):
```json
{
  "taskId": "<UUID>",
  "status": "status"
}
```

Структура webhook запроса:
```json
{
    "taskId": "<UUID>",
    "file": "http://...",
    "payload": "",
    "status": "status",
    "expiredAt": 1602754700
}
```

Если не передать/передать неверный ключ авторизации, в ответ придет 400 код с телом (поле auth в заголовках):
```json
{
  "status":"Error"
}
```

### Список роутов:
_[POST]_ __*__ ```/``` - основной роут для генерации данных.

Данные в этом случае должны быть переданы как json:
```shell
curl -H "Accept: application/json" -H "Content-Type: application/json" --data @var/data.json.gz http://localhost:8000/gzip
# или
curl -H "Accept: application/json" -H "Content-Type: application/json" --data '{"sheets": {..}, "formats": {..}, "payload": "any-string"}' http://localhost:8000/gzip
```

_[POST]_ __*__ ```/gzip``` - основной роут для генерации данных (принимает архивированные данные. Для экономии трафика).

Данные в этом случае должны передаваться как форма (пример):
```shell
curl -H "Accept: application/json" -H "Content-Type: multipart/form-data" -F file=@var/data.json.gz http://localhost:8000/gzip
```

_[GET]_ ```/docs``` - документация приложения.

_[GET/HEAD]_ ```/health``` - роут для проверки доступности приложения.

_[GET]_ __*__ ```/queue``` - вернёт информацию об очереди.

_[GET]_ __*__ ```/status/<task_id>``` - вернёт информацию о задаче.

_[GET]_ ```/version``` - вернёт версию приложения.

__*__ - требует аутентификации. (см. **api_key** в **config.yml**)