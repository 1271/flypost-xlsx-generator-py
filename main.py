from xlsx_builder import *


if __name__ == '__main__':
    import sys

    print('Please, run server with "uvicorn main:app"', file=sys.stderr)
    print(f'Daemon version: {version}', file=sys.stderr)
    exit(1)
else:
    from xlsx_builder.app import app
    from xlsx_builder.app import logger
    logger.info(f'App version: {app.version}')

