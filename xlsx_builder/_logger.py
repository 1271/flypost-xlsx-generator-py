from logging import getLogger, Formatter, StreamHandler, FileHandler, DEBUG, INFO, WARNING, ERROR
from pathlib import Path
from sys import stderr

__all__ = ['config_logger', 'get_log_level', 'get_logger']


def get_logger(name: str = 'xlsx_generator'):
    return getLogger(name)


def get_log_level(level: str) -> int:
    if 'debug' == level.lower():
        return DEBUG
    if 'info' == level.lower():
        return INFO
    if 'warning' == level.lower():
        return WARNING
    return ERROR


def config_logger(max_level: int, file: str = '', name: str = 'xlsx_generator'):
    logger = get_logger(name)
    logger.setLevel(DEBUG)

    formatter = Formatter('%(asctime)s:%(name)s - %(levelname)s - %(message)s')

    if '' == file:
        handler = StreamHandler(stream=stderr)
    else:
        Path(file).absolute().parent.mkdir(parents=True, exist_ok=True)
        handler = FileHandler(filename=file)

    handler.setLevel(max_level)
    handler.setFormatter(formatter)

    for _handler in logger.handlers:
        logger.removeHandler(_handler)

    logger.addHandler(handler)

    return logger
