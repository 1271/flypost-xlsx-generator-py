import time
from gzip import decompress
from os import getenv
from pathlib import Path
from queue import Queue
from re import compile
from threading import Thread
from traceback import print_tb
from typing import Dict, Union, Optional
from uuid import UUID, uuid4

import requests
from PIL import ImageFont
from fastapi import BackgroundTasks, FastAPI, Request, File
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from fastapi.responses import Response
from pydantic import ValidationError, BaseModel

from . import models
from ._logger import config_logger, get_log_level
from .config import config
from .config import root_path, var_path
from .meta import version
from .models_storage import *
from .utils import FILES_TTL_STORAGE, builder

RE_URL = compile(r'^https?://\w')


logger = config_logger(get_log_level(str(config.get('log_level', 'debug'))), str(config.get('log_file', '')))
# логирование ошибок валидации теле запроса
logger_validation = config_logger(
    get_log_level(str(config.get('log_level', 'debug'))),
    str(config.get('log_file_validation', '')),
    'xlsx_generator_validation',
)

app = FastAPI(title='xlsx generator', version=version)
storage_ttl = int(config.get('storage_ttl', 0))
skip_request_validation = bool(config.get('skip_request_validation', False))

try:
    response_errors_limit = int(config.get('validation_errors_limit', 10))
    if response_errors_limit < 1:
        raise ValueError('bad value')
except ValueError:
    logger.error('config.validation_errors_limit is value')
    response_errors_limit = 10


def attempt_step_multiplier(val: int) -> float:
    timeout = float(config.get('delivery_attempts_reply_timeout'))
    return (1.2 ** val) * timeout


if config.get('storage_cleanup_enabled', False):
    def clear_cache():
        while True:
            time.sleep(5)
            tasks = FILES_TTL_STORAGE.keys()
            to_delete = []
            to_increase_ttl = []
            for task_id in tasks:
                task = FILES_TTL_STORAGE[task_id]
                expired_at = task.get('expiredAt', -1)

                if not task.get('done'):
                    to_increase_ttl.append(task_id)
                    continue

                if expired_at is None or expired_at - time.time() > 0:
                    continue

                try:
                    var_path.joinpath(f'{task_id}.xlsx').unlink()
                except FileNotFoundError:
                    pass

                to_delete.append(task_id)

            for task_id in to_delete:
                logger.info('Delete file for %s' % task_id)
                FILES_TTL_STORAGE.pop(task_id)

            # пока задача висит в очереди, она не должна умереть
            for task_id in to_increase_ttl:
                FILES_TTL_STORAGE[task_id]['expiredAt'] = time.time() + storage_ttl

    background_worker = Thread(daemon=True, target=clear_cache)
    background_worker.start()


class QueueWrapper:
    class Daemon(Thread):
        def __init__(self, queue, q_id=0, *args, daemon=True, **kwargs):
            super().__init__(*args, daemon=daemon, **kwargs)
            self.queue = queue
            self.q_id = q_id

        def run(self):
            while True:
                try:
                    callback, args, kwargs = self.queue.get()
                    logger.info(f'Task {args[0]} start / {self.q_id}')
                    callback(*args, **kwargs)
                    self.queue.task_done()
                    logger.info(f'Task {args[0]} done / {self.q_id}')
                except Exception:
                    self.queue.task_done()

    def __init__(self, max_threads=1):
        self._queue = Queue()

        for i in range(max_threads):
            t = self.Daemon(self._queue, i)
            t.start()
            logger.info(f'Thread {i}: started')

        logger.info('Queue started')

    @staticmethod
    def error_callback(task_id, exception):
        logger.warning(f'Task "{task_id}" error: "{exception}"')
        status = 'Processing error'
        FILES_TTL_STORAGE[f'{task_id}'].update({
            'status': status,
            'expiredAt': int(time.time() + storage_ttl) if storage_ttl > 0 else None,
            'done': True,
        })
        webhook_url = str(config.get('webhook_url', False))
        base_url = str(config.get('webhook_base_url', False))
        if webhook_url and base_url and RE_URL.search(webhook_url) is not None:
            attempts = int(config.get('max_report_delivery_attempts'))
            attempt = 0
            while attempt < attempts:
                try:
                    requests.post(webhook_url, json={
                        'taskId': f'{task_id}',
                        'status': status,
                        'payload': FILES_TTL_STORAGE[f'{task_id}'].get('payload'),
                        'expiredAt': int(time.time() + 30) if storage_ttl > 0 else None,
                    })
                    logger.warning(f'Webhook send success "{task_id}"')
                    break
                except requests.exceptions.ConnectionError as e:
                    time.sleep(attempt_step_multiplier(attempt))
                    logger.warning('Webhook connection error!')

                if attempt == 0:
                    FILES_TTL_STORAGE[f'{task_id}'].update({
                        'expiredAt': time.time() + 30,
                    })

                attempt += 1
        unlink_model(task_id)

    @staticmethod
    def callback(task_id, *args, **kwargs):
        webhook_url = str(config.get('webhook_url', ''))
        base_url = str(config.get('webhook_base_url', ''))
        if webhook_url and base_url and RE_URL.search(webhook_url) is not None:
            attempts = int(config.get('max_report_delivery_attempts', 5))
            attempt = 0
            while attempt < attempts:
                file = f'{base_url}/{task_id}.xlsx'
                try:
                    requests.post(webhook_url, json={
                        'taskId': f'{task_id}',
                        'status': 'Ok',
                        'file': file,
                        'expiredAt': int(time.time() + storage_ttl) if storage_ttl > 0 else None,
                        'payload': FILES_TTL_STORAGE[f'{task_id}'].get('payload'),
                    })
                    logger.info(f'Webhook send success "{task_id}"')
                    FILES_TTL_STORAGE[f'{task_id}'].update({
                        'status': 'Webhook error',
                        'expiredAt': int(time.time() + storage_ttl) if storage_ttl > 0 else None,
                        'file': file,
                        'done': True,
                    })
                    break
                except requests.exceptions.ConnectionError as e:
                    logger.warning('Webhook connection error!')
                    time.sleep(attempt_step_multiplier(attempt))
                attempt += 1
        unlink_model(task_id)

    def add(self, task_id: UUID):
        _builder = builder(
            config.get('allow_format', False),
            default_font_name=default_font_name,
            default_font_size=default_font_size,
            font_corrector=font_corrector,
            font_corrector_h=font_corrector_h,
            xlsx_file_path=xlsx_file_path,
            calc_cell_width=calc_cell_width,
            unlink_bad_data=config.get('unlink_bad_data', True)
        )
        self._queue.put_nowait((
            _builder,
            (task_id,),
            {'callback': self.callback, 'error_callback': self.error_callback}
        ))

    @property
    def queue(self) -> Queue:
        return self._queue

    def join(self):
        self._queue.join()


def xlsx_file_path(task_id: UUID) -> Path:
    return var_path.joinpath(f'{task_id}.xlsx')


default_font_name = 'Droid Sans'
default_font_size = int(config.get('default_font_size', 11))


def make_font(font_name: str, size: int):
    return ImageFont.truetype(font=font_name, size=size)


font = str(root_path.joinpath('fonts', 'DroidSans', 'DroidSans.ttf'))
font_bold = str(root_path.joinpath('fonts', 'DroidSans', 'DroidSans-Bold.ttf'))
FONTS: Dict[str, ImageFont.FreeTypeFont] = {
    f'n{default_font_size}': make_font(font, default_font_size),
    f'b{default_font_size}': make_font(font_bold, default_font_size),
}

font_corrector = 6.1
font_corrector_h = 11 / 14


def calc_cell_width(text: str, size: Union[int, float] = default_font_size, bold: bool = False) -> (int, int):
    size = int(size)
    try:
        _size = '{}{}'.format('b' if bold else 'n', size)
        _font = FONTS.get(_size, None)
        if _font is None:
            FONTS[_size] = _font = make_font(font_bold if bold else font, size)
        return _font.getsize(text)
    except Exception as e:
        logger.error(f'calc_cell_width error "{e}"')
        raise e


# смысл очередей в том, чтобы разгрузить основной поток приложения и чтобы мы могли принимать новые задачи
#  даже в процессе обработки фоновых данных
queue = QueueWrapper()


def calc_rows_count(sheets) -> int:
    return sum([sum(map(len, x.values())) for x in sheets.values() if type(x) == dict])


async def new_xlsx(task_id: UUID, model: Optional[models.MainModel] = None, data: dict = None):
    try:
        logger.info('pre-dump')

        if model is not None:

            sheets = model.sheets
            payload = model.payload

            dump_model(task_id, model)
        elif data is not None:

            sheets = data['sheets']
            payload = data['payload']

            dump_json(task_id, data)
        else:
            raise ValidationError('Empty data')

        logger.info('post-dump')

        FILES_TTL_STORAGE[f'{task_id}'].update({
            'payload': payload,
            'status': 'Task created',
            'expiredAt': int(time.time() + storage_ttl) if storage_ttl > 0 else None,
        })

        if len(sheets.keys()) == 0:
            logger.info(f'Empty sheets for task "{task_id}"')

            FILES_TTL_STORAGE[f'{task_id}'].update({
                'status': 'Empty sheets',
                'expiredAt': int(time.time() + 30),
            })

            return

        queue.add(task_id)

        logger.info(f'New task: {task_id} / Rows: {calc_rows_count(sheets)}')
    except Exception as e:
        logger.info(f'New task: {task_id} / Error')
        if getenv('PRINT_TRACEBACK', '') == 'true':
            print_tb(e.__traceback__)
        logger.warning(e.__str__())


logger.info('Config - success')


def validation_error_to_response(exception: ValidationError):
    id_ = uuid4().__str__()
    errors = [{'loc': err.get('loc', ''), 'msg': err.get('msg', '')} for err in exception.errors()]

    if len(errors) > response_errors_limit:
        errors = [*errors[0:response_errors_limit], {'loc': '...', 'msg': '...'}]

    logger.warning('Unprocessable entity: %s / %s' % (jsonable_encoder(errors), id_))
    logger_validation.warning(jsonable_encoder({'id': id_, 'details': exception.errors()}))
    return JSONResponse(
        status_code=422,
        content=jsonable_encoder({'message': '422 Unprocessable Entity', 'details': errors, 'id': id_}),
    )


async def make_background_task(
        background_tasks: BackgroundTasks,
        data: Optional[dict] = None,
) -> models.MainResponseModel:
    task_id = uuid4()

    if not skip_request_validation:
        models.MainModel.parse_obj(data)

    position = len([1 for pid in FILES_TTL_STORAGE if not FILES_TTL_STORAGE[pid]['done']])

    background_tasks.add_task(new_xlsx, task_id, data=data)
    FILES_TTL_STORAGE[f'{task_id}'] = {
        'taskId': task_id,
        'status': 'queue',
        'expiredAt': int(time.time() + storage_ttl) if storage_ttl > 0 else None,
        'createdAt': time.time(),
        'done': False,
    }
    return models.MainResponseModel(taskId=task_id, status='queue', position=position, )


@app.exception_handler(ValidationError)
async def validation_exception_handler(request: Request, exc: ValidationError):
    return validation_error_to_response(exc)


@app.route('/', ['GET'])
async def index_route(req):
    return Response('', 302, headers={
        'Location': '/docs'
    })


@app.get('/health', response_model=models.EmptyModel)
@app.head('/health', status_code=204)
async def health_route():
    return {}


@app.get('/status/{task_id}', response_model=Union[models.StatusModel, models.ErrorModel])
async def status_route(task_id: UUID, request: Request, response: Response):
    if config.get('api_key', '') != request.headers.get('auth', ''):
        logger.info(f'Auth error "{request.client.host}" / {request.headers.get("user-agent")}')
        response.status_code = 400
        return models.ErrorModel(status='Auth error')

    key = f'{task_id}'
    if key not in FILES_TTL_STORAGE:
        response.status_code = 404
        return {'status': 'Not found'}

    return models.StatusModel(**FILES_TTL_STORAGE[key])


@app.post(
    '/',
    response_model=Union[models.MainResponseModel, models.ErrorModel],
    description='Make xlsx from base model',
)
async def generate_route(
        background_tasks: BackgroundTasks, data: dict, request: Request, response: Response
) -> BaseModel:
    if config.get('api_key', '') != request.headers.get('auth', ''):
        logger.info(f'Auth error "{request.client.host}" / {request.headers.get("user-agent")}')
        response.status_code = 400
        return models.ErrorModel(status='Auth error')
    return await make_background_task(background_tasks, data=data)


@app.post(
    '/gzip',
    response_model=Union[models.MainResponseModel, models.ErrorModel],
    description='Make xlsx from gzip-compressed model',
)
async def generate_route(
        background_tasks: BackgroundTasks, request: Request, response: Response, file: bytes = File(...)
) -> Union[BaseModel, JSONResponse]:
    if config.get('api_key', '') != request.headers.get('auth', ''):
        logger.info(f'Auth error "{request.client.host}" / {request.headers.get("user-agent")}')
        response.status_code = 400
        return models.ErrorModel(status='Auth error')

    try:
        content = decompress(file)
        return await make_background_task(background_tasks, data=content)
    except ValidationError as e:
        return validation_error_to_response(e)
    except Exception:
        return models.ErrorModel(status='Bad gzip content')


@app.route('/version', ['GET'])
async def get_version(req):
    return Response(version)


__all__ = ['app']
