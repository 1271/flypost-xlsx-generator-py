from pathlib import Path

from yaml import safe_load

__all__ = ['root_path', 'var_path', 'config']


root_path = Path(__file__).parent.parent.resolve()


with root_path.joinpath('config.yml').open('r') as r:
    config = safe_load(r)

try:
    with root_path.joinpath('config.local.yml').open('r') as r:
        _cfg = safe_load(r)
        config.update(_cfg)
except FileNotFoundError:
    pass


storage_dir = config.get('storage_dir', None)
var_path = Path(storage_dir).resolve() if storage_dir is not None else root_path.joinpath('var')
var_path.mkdir(0o755, parents=True, exist_ok=True)

