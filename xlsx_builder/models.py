from typing import Optional, Dict, Union
from uuid import UUID

from pydantic import BaseModel, Field, constr, confloat

__all__ = ['EmptyModel', 'MainModel', 'MainResponseModel', 'TasksCountModel', ]

color_field = Field(description='Example: "red" / "#ff00a0"')


class EmptyModel(BaseModel):
    pass


class FontModel(BaseModel):
    bold: bool = False
    italic: bool = False
    underline: bool = False
    alignment: Optional[str] = Field(regex=r'left|center|right|justify|fill')
    valignment: Optional[str] = Field(regex=r'top|vcenter|bottom')
    color: Optional[str] = color_field
    size: Optional[confloat(gt=5, lt=33)] = None


class CellModel(BaseModel):
    value: Union[str, int] = Field(description='Cell value')
    format: Optional[str] = Field(description='Format key')
    formula: Optional[str] = Field(description='Calculated cell value')


class BorderModel(BaseModel):
    color: Optional[str] = color_field
    type: int = Field(description='See https://docs.microsoft.com/en-us/office/vba/api/excel.xllinestyle')


class FormatModel(BaseModel):
    font: Optional[FontModel] = None
    bg_color: Optional[str] = color_field
    border: Optional[Dict[str, BorderModel]] = Field(description='Key value pattern: "left|right|top|bottom"')


class MainModel(BaseModel):
    options: Optional[FontModel] = Field(description='Default font style')
    payload: Optional[constr(max_length=255)] = Field(description='Any string')
    formats: Optional[Dict[constr(max_length=16), FormatModel]] = Field(
        description='Format key is used for cells format'
    )
    sheets: Dict[str, Dict[str, Dict[str, Union[CellModel, str, int, float]]]] = Field(
        description='Top-level is sheet name. Middle level is the line number. Internal - column number'
    )

    class Config:
        schema_extra = {
            "example": {
                "payload": "my-payload",
                "formats": {
                    "fmt1": {
                        "font": {
                            "bold": True,
                            "italic": True,
                            "alignment": "center",
                            "color": "#f0a0c0"
                        },
                        "border": {
                            "left": {
                                "color": "#FF0000",
                                "type": 1
                            }
                        }
                    }
                },
                "sheets": {
                    "book 1": {
                        "0": {
                            "0": {
                                "value": "tExt",
                                "format": "fmt1"
                            },
                            "2": {
                                "value": "tExt",
                                "format": "fmt1"
                            },
                            "3": {
                                "value": "=c1",
                                "formula": "refresh it, please"
                            },
                            "4": "=CONCAT(C1, C2)"
                        },
                        "1": {
                            "2": {
                                "value": "C2 text",
                                "format": "fmt1"
                            }
                        },
                        "2": {
                            "2": {
                                "value": "a123"
                            },
                            "5": "value cell 5 row 2",
                            "6": 12345
                        }
                    }
                }
            }
        }


class MainResponseModel(BaseModel):
    taskId: UUID
    status: str
    position: int


class StatusModel(BaseModel):
    taskId: UUID
    status: str
    createdAt: int
    file: Optional[str] = None
    expiredAt: Optional[int] = None
    startedAt: Optional[None] = None


class ErrorModel(BaseModel):
    status: str


class TasksCountModel(BaseModel):
    queue: int
    another: int
