import json

from .models import MainModel
from uuid import UUID
from .config import var_path


__all__ = ['dump_model', 'load_model', 'unlink_model', 'dump_json']


def _path(task_id: UUID):
    return var_path.joinpath(f'.{task_id}.json')


def dump_model(task_id: UUID, model: MainModel):
    _path(task_id).write_text(model.json())


def dump_json(task_id: UUID, data: dict):
    _path(task_id).write_text(json.dumps(data))


def load_model(task_id: UUID) -> MainModel:
    model = MainModel.parse_file(str(_path(task_id)))
    return model


def unlink_model(task_id: UUID):
    _path(task_id).unlink()
