import time
from os import getenv
from traceback import print_tb
from typing import Dict, Union
from uuid import UUID

import xlsxwriter

from .._logger import get_logger
from ..models import CellModel
from ..models_storage import load_model, unlink_model

logger = get_logger()
TypeOptions = Dict[str, Dict[str, Union[str, int]]]
TypeData = Dict[str, Dict[str, Dict[str, CellModel]]]

FILES_TTL_STORAGE = {}  # type: Dict[str, dict]


def _workbook(options, default_font_name, _font_size, xlsx_file_path, task_id, **kwargs):
    _font = options.get('font', {})
    global_options = {
        'font_name': default_font_name,
        'font_size': _font.get('size', _font_size),
    }
    if _font.get('style', 'normal') == 'bold':
        global_options['bold'] = True

    return xlsxwriter.Workbook(xlsx_file_path(task_id), options={
        'in_memory': False,
        'default_format_properties': global_options,
        'use_zip64': True,
        'constant_memory': True,
    }), global_options


def builder(
        full=False, *,
        default_font_name, default_font_size, font_corrector,
        font_corrector_h, xlsx_file_path, calc_cell_width,
        unlink_bad_data: bool = True
):
    if full:
        from . import xlsx_builder_fmt as _b
    else:
        from . import xlsx_builder as _b

    _b.default_font_name = default_font_name
    _b.default_font_size = default_font_size
    _b.font_corrector = font_corrector
    _b.font_corrector_h = font_corrector_h
    _b.xlsx_file_path = xlsx_file_path
    _b.calc_cell_width = calc_cell_width

    def print_error(task_id, callback, e):
        logger.warning(f'Builder error: {e}')

        FILES_TTL_STORAGE[f'{task_id}'].update({
            'status': 'Processing:error',
            'expiredAt': time.time() + 300,
        })

        logger.warning(f'Error callback: {callable(callback)} / {e}')

        if callable(callback):
            callback(e)

    def xlsx_builder(task_id: UUID, callback, error_callback):
        """
        :param task_id:
        :param options:
        :param data:
        :param callback:
        :param error_callback:
        :param args:
        :param kwargs:
        :return:
        """

        try:
            model = load_model(task_id)
        except Exception as e:
            print_error(task_id, error_callback, e)

            if unlink_bad_data:
                unlink_model(task_id)

            return

        options = model.options or {
            'font': {
                'size': default_font_size,
                'style': 'normal',  # normal/bold
            },
        }

        FILES_TTL_STORAGE[f'{task_id}'].update({
            'status': 'Processing',
            'expiredAt': time.time() + 300,
            'startedAt': time.time(),
        })

        try:
            start_time = time.time()
            logger.info(f'Before builder "{task_id}"')
            _b._builder(task_id=task_id, options=options, data=model.sheets, formats=model.formats)
            logger.info(f'After builder "{task_id}". Time: {int((time.time() - start_time) * 10) / 10}s')

            FILES_TTL_STORAGE[f'{task_id}'].update({
                'status': 'Processing:done',
                'expiredAt': time.time() + 300,
            })

            if callable(callback):
                callback(task_id)

        except Exception as e:
            if getenv('PRINT_TRACEBACK', '') == 'true':
                print_tb(e.__traceback__)

            print_error(task_id, error_callback, e)


    return xlsx_builder
