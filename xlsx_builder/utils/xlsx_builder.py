from typing import Dict
from uuid import UUID

from . import TypeData, TypeOptions, _workbook
from .._logger import get_logger

default_font_name: str = ''
default_font_size: int = 0
font_corrector: float = .0
font_corrector_h: float = .0
xlsx_file_path: callable = lambda *args: args
calc_cell_width: callable = lambda *args: args

logger = get_logger()

ROWS = {}


def _builder(task_id: UUID, options: TypeOptions, data: TypeData, **kwargs):
    workbook, _ = _workbook(options, default_font_name, default_font_size, xlsx_file_path, task_id)

    _fmt = workbook.add_format({
        'font_size': default_font_size
    })

    for name in data:
        logger.debug(f'New sheet "{name}"')
        worksheet = workbook.add_worksheet(name)
        max_width: Dict[int, str] = {}

        logger.debug(f'Rows: {len(data[name])}')
        for row in data[name]:
            line = data[name][row]

            logger.debug(f'Cells: {len(line)}')

            for cell in line:  # type: str
                value = line[cell]
                _cell = int(cell)

                w_content = max_width.get(_cell, '')

                if type(value) == str:
                    _value = value
                    formula_data = 0

                else:
                    _value = str(value.value)
                    formula_data = value.formula or 0

                if _value.startswith('=') or (_value.startswith('{=') and _value.endswith('}')):
                    if formula_data != 0 and len(str(formula_data)) > len(str(w_content)):
                        max_width[_cell] = formula_data

                    worksheet.write(int(row), _cell, _value, None, formula_data)
                else:
                    if len(_value) > len(w_content):
                        max_width[_cell] = _value

                    worksheet.write(int(row), _cell, _value)

            ROWS[row] = default_font_size

        for key in max_width:
            width, height = calc_cell_width(max_width[key], default_font_size)
            worksheet.set_column(key, key, width / font_corrector + .25, )

        for row in ROWS:
            width, height = calc_cell_width('cell', default_font_size)
            worksheet.set_row(int(row), height / font_corrector_h / 1.2, _fmt)

    workbook.close()
