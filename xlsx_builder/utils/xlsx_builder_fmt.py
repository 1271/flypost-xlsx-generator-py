from copy import deepcopy
from typing import Dict, Tuple
from uuid import UUID

from xlsxwriter import Workbook

from . import TypeData, TypeOptions, _workbook
from .._logger import get_logger
from ..config import root_path
from .. import models

default_font_name: str = ''
default_font_size: int = 0
font_corrector: float = .0
font_corrector_h: float = .0
xlsx_file_path: callable = lambda *args: args
calc_cell_width: callable = lambda *args: args

logger = get_logger()

bold_font = str(root_path.joinpath('fonts', 'DroidSans', 'DroidSans-Bold.ttf'))

# font sizes
FORMAT_FS: Dict[str, Tuple[float, bool]] = {}


def _format(key: str, options: models.FormatModel, workbook: Workbook, formats: dict):
    _options = {}

    if key in formats.keys():
        return formats[key]

    if options.font is not None:
        font = options.font

        size = font.size or 0.
        if size > 0:
            _options['font_size'] = size

        bold = font.bold
        italic = font.italic
        underline = font.underline

        if key not in FORMAT_FS:
            FORMAT_FS[key] = (size if size > 0 else default_font_size), bold

        if bold:
            _options['bold'] = 1
        if italic:
            _options['italic'] = 1
        if underline:
            _options['underline'] = 1

        alignment = font.alignment
        if alignment in ['left', 'center', 'right', 'justify', 'fill']:
            _options['align'] = alignment

        alignment = font.valignment
        if alignment in ['top', 'vcenter', 'bottom']:
            _options['valign'] = alignment

        color = font.color
        if color is not None:
            _options['font_color'] = color

    bg_color = options.bg_color
    if bg_color is not None:
        _options['bg_color'] = bg_color

    border = options.border

    if border is not None:
        def _set_border(side):
            data = border.get(side)
            if data is None:
                return
            _style = data.type
            _color = data.color
            if _style is not None:
                _options[side] = _style
            if _color is not None:
                _options[f'{side}_color'] = _color
        [_set_border(side) for side in ['bottom', 'top', 'left', 'right']]

    fmt = workbook.add_format(_options)
    formats[key] = fmt


def build_formats(data: TypeData, workbook: Workbook, formats: dict):
    _formats = data.get('formats', {})
    for idx in _formats:
        fmt = _formats[idx]
        if isinstance(fmt, models.FormatModel):
            _format(idx, fmt, workbook, formats)


def _builder(task_id: UUID, options: TypeOptions, data: TypeData, **kwargs):
    # sheet formats
    formats = {}

    workbook, global_options = _workbook(options, default_font_name, default_font_size, xlsx_file_path, task_id)
    build_formats(kwargs, workbook, formats)

    for name in data:
        # rows height settings
        _rows = {}

        logger.debug(f'New sheet "{name}"')
        worksheet = workbook.add_worksheet(name)
        max_width: Dict[int, Tuple[str, int, bool]] = {}

        logger.debug(f'Rows: {len(data[name])}')
        for row in data[name]:
            line = data[name][row]

            row_options = deepcopy(global_options)
            row_options.update(line.get('options', {}))

            logger.debug(f'Cells: {len(line)}')

            _fs_row = None

            for cell in line:  # type: str
                value = line[cell]

                if type(value) == str:
                    key = ''
                    formula_data = 0
                    _value = value

                else:
                    key = value.format or ''
                    _value = str(value.value)
                    formula_data = value.formula or 0

                _cell = int(cell)

                _fs = FORMAT_FS.get(key, None)
                _fs_row = _fs_row or _fs
                _font_size, is_b = (default_font_size, False) if _fs is None else _fs

                w_content, _f_size, _is_b = max_width.get(_cell, ('', 1, is_b))

                fmt = formats.get(key)

                # чтобы не вычислять ширину каждой ячейки, делаем "хак"
                multiply = 1.05 if is_b else 1
                _multiply = 1.05 if _is_b else 1

                if _value.startswith('=') or (_value.startswith('{=') and _value.endswith('}')):

                    _cur_w = len(str(formula_data)) * _font_size * multiply
                    _ex_w = len(w_content) * _f_size * _multiply

                    if formula_data != 0 and _cur_w > _ex_w:
                        max_width[_cell] = formula_data, _font_size, is_b

                    worksheet.write(int(row), _cell, _value, fmt, formula_data)
                else:
                    _cur_w = len(_value) * _font_size * multiply
                    _ex_w = len(w_content) * _f_size * _multiply

                    if _cur_w > _ex_w:
                        max_width[_cell] = _value, _font_size, is_b

                    worksheet.write(int(row), _cell, _value, fmt)

                    _rows[row] = max(_rows.get(row, 0), _font_size)

            if _fs_row is not None:
                width, height = calc_cell_width('cell', _rows[row])
                result = worksheet.set_row(int(row), height / font_corrector_h)

                logger.info(f'set height {row}: {height} / {font_corrector_h} {height / font_corrector_h} / {result}')

        for key in max_width:
            width, height = calc_cell_width(*max_width[key])
            worksheet.set_column(key, key, width / font_corrector + .25, )

    workbook.close()
